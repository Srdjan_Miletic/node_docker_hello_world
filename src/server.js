const express = require('express');
const handlebars = require('handlebars')

const PORT = 8080;
const HOST = '0.0.0.0';

const app = express();
app.set('view engine', 'hbs');
app.get('/', (req, res) => {
    res.status(200).send('Hello World');
});

app.get('/hbs', (req,res)=>{
    res.render('homePage', { body: "I'm a handlebars template!!!"})
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);