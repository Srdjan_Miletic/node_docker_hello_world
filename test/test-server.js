const chai = require('chai');
const expect = chai.expect;
const request = require('request');
const fs = require('fs');

const server = require('../src/server');

it('Should say hello', (done) => {
    request('http://localhost:8080', (error, response, body) => {
        expect(response.statusCode).to.equal(200);
        expect(body).to.equal('Hello World');
        done();
    })
});

it('Should use a hbs template', (done) => {
    request('http://localhost:8080/hbs', (error, response, body) => {
        expect(response.statusCode).to.equal(200);
        expect(body).to.equal();
        done();
    })
});